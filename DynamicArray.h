/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DynamicArray.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 2:54 PM
 */

#ifndef DYNAMICARRAY_H
#define DYNAMICARRAY_H

template <class T> class DynamicArray{
    public:    
        DynamicArray();
        ~DynamicArray();
        
        void append(T);
        void append(T*, int);
        T* getRawData();
        int getLength();
    private:
        const static int DEFAULT_SIZE;
        
        T* _data;
        int _length;
        int _internal_length;
        
        void resize();
};
template <class T> const int DynamicArray<T>::DEFAULT_SIZE = 50;

template <class T> DynamicArray<T>::DynamicArray(){
    _data = new T[DEFAULT_SIZE];
    _internal_length = DEFAULT_SIZE;
    _length = 0;
}

template <class T> DynamicArray<T>::~DynamicArray(){
    delete[] _data;
}

template <class T> void DynamicArray<T>::append(T data){
    _data[_length++] = data;
    if(_length == _internal_length){
        resize();
    }
}

template <class T> void DynamicArray<T>::append(T* data, int length){
    while (_length + length > _internal_length){
        resize();
    }
    memcpy(_data + _length, data ,length);
    _length += length;
    if(_length == _internal_length){
        resize();
    }
}
template <class T> void DynamicArray<T>::resize(){
    int newSize = _internal_length * 2;
    T* temp = new T[newSize];
    memcpy(temp,_data, _length);
    delete[] _data;
    _data = temp;
    _internal_length = newSize;
}

template <class T> T* DynamicArray<T>::getRawData(){
    return _data;
}

template <class T> int DynamicArray<T>::getLength(){
    return _length;
}

#endif /* DYNAMICARRAY_H */

