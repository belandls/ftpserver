
/* 
 * File:   Command.cpp
 * Author: Samuel
 * Description : Implementation of the Command Class
 * Created on February 9, 2016, 11:18 AM
 */

#include "Command.h"

Command::Command() {
}

Command::~Command() {
}

void Command::setWorkingSocket(SOCKET s){
	_s = s;
}

SOCKET Command::getWorkingSocket(){
	return _s;
}
