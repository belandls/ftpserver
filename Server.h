/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Server.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 11:20 AM
 */

#ifndef SERVER_H
#define SERVER_H
#define _WIN32_WINNT 0x501
#include<WinSock2.h>
#include <ws2tcpip.h>
#pragma comment (lib, "Ws2_32.lib")


class Server {
public:
    Server();
    
    virtual ~Server();
    bool init();
    void mainListenLoop();
private:
    const static char* DEFAULT_PORT;
    SOCKET _listen_socket;
};

#endif /* SERVER_H */

