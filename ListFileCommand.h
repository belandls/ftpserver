/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ListFileCommand.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 11:20 AM
 */

#ifndef LISTFILECOMMAND_H
#define LISTFILECOMMAND_H
#include "Command.h"
#include "DynamicArray.h"

class ListFileCommand : public Command {
public:
    ListFileCommand();
    ListFileCommand(const ListFileCommand& orig);
    virtual ~ListFileCommand();


    virtual bool executeCommand(const char*);
    virtual bool performDataTransfer();
    virtual bool performInitialNego();



private:
    DynamicArray<char> _directory_data;
    
};

#endif /* LISTFILECOMMAND_H */

