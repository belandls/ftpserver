/* 
 * File:   GetFileCommand.cpp
 * Author: Samuel Beland-Leblanc - 27185642
 * Description : GetFileCommand class implementation
 * Created on February 14, 2016, 3:37 PM
 */

#include "GetFileCommand.h"
#include "Request.h"
#include <iostream>
using namespace std;
GetFileCommand::GetFileCommand() {
}

GetFileCommand::GetFileCommand(const GetFileCommand& orig) {
}

GetFileCommand::~GetFileCommand() {
    delete _file_chunker;
}

//Tries to load the file to send to the client
bool GetFileCommand::executeCommand(const char* addInfo) {

    _file_chunker = new FileChunker();
    if (_file_chunker->loadFile(addInfo)){
        //If the file could be loaded, send a success command
        int res;
        Request sr(Request::COMMAND_SUCCESFULL,0);
        res = send(getWorkingSocket(), sr.getRawByteRepresentation(), Request::REQUEST_SIZE, 0);
        if (res == SOCKET_ERROR){
            return false;
        }
        return true;
    }else{
        //If the file could not be loaded, send an error command
        int res;
        Request sr(Request::FILE_NOT_FOUND,0);
        res = send(getWorkingSocket(), sr.getRawByteRepresentation(), Request::REQUEST_SIZE, 0);
    }
    return false;
}

//Send the file by chunks
bool GetFileCommand::performDataTransfer() {

    int res;
    //While the EOF has not been met
    while(!_file_chunker->isEOF()){
        //Send the next chunk
        const char* data = _file_chunker->getNextChunk();
        int dataSize = _file_chunker->getLastChunkSize();
        res = send(getWorkingSocket(), data,dataSize,0);
        if (res == SOCKET_ERROR){
            return false;
        }
    }
    
    return true;
    
}

bool GetFileCommand::performInitialNego() {

    //First send a READY_TO_SEND with the filesize
    int res;
    Request sr(Request::READY_TO_SEND, _file_chunker->getFileSize());
    res = send(getWorkingSocket(), sr.getRawByteRepresentation(), Request::REQUEST_SIZE,0);
    if (res == SOCKET_ERROR){
        return false;
    }
    
    
    //After check if the client is ready to receive
    char buffer[Request::REQUEST_SIZE];
    Request rr;
    res= recv(getWorkingSocket(),buffer,Request::REQUEST_SIZE,0);
    if (res == SOCKET_ERROR){
        return false;
    }
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::READY_TO_RECV){
        //If the client had an error, abort the command execution
        cout << "There was an unexpected error on the client's side" << endl;
        return false;
    }
    return true;
}

