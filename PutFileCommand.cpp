/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PutFileCommand.cpp
 * Author: Sam
 * 
 * Created on February 10, 2016, 5:16 PM
 */

#include "PutFileCommand.h"
#include "DynamicArray.h"
#include "Request.h"

PutFileCommand::PutFileCommand() {
}

PutFileCommand::PutFileCommand(const PutFileCommand& orig) {
}

PutFileCommand::~PutFileCommand() {
}

bool PutFileCommand::executeCommand(const char* addInfo) {

    _out_stream = new ofstream(addInfo, ofstream::binary);
    if (_out_stream->is_open()) {

        Request r(Request::COMMAND_SUCCESFULL, 0);
        int res = send(getWorkingSocket(), r.getRawByteRepresentation(), Request::REQUEST_SIZE, 0);
        if (res == SOCKET_ERROR) {
            return false;
        }

        return true;
    }else{
        Request r(Request::FILE_NOT_CREATED, 0);
        send(getWorkingSocket(), r.getRawByteRepresentation(), Request::REQUEST_SIZE, 0);
    }
    return false;
}

bool PutFileCommand::performDataTransfer() {

    int receivedData = 0;
    int res;
    char buffer[4096];
    while (receivedData < _size_of_data) {
        res = recv(getWorkingSocket(), buffer, 4096, 0);
        if (res == SOCKET_ERROR) {
            return false;
        } else {
            _out_stream->write(buffer, res);
            receivedData += res;
        }
    }
    _out_stream->close();
    return true;

}

bool PutFileCommand::performInitialNego() {

    int res;
    Request rr;
    char buffer[260];
    res = recv(getWorkingSocket(), buffer, 260, 0);
    if (res == SOCKET_ERROR) {
        return false;
    }
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::READY_TO_SEND) {
        return false;
    }
    _size_of_data = rr.getSizeOfData();
    Request sr(Request::READY_TO_RECV, 0);
    res = send(getWorkingSocket(), sr.getRawByteRepresentation(), 260, 0);
    if (res == SOCKET_ERROR) {
        return false;
    }
    return true;
}
